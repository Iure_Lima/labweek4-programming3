using System;
using System.IO;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;


using LibVLCSharp.Shared;


namespace LabWeek4.Views;

public partial class MainWindow : Window
{
    private readonly Control _bottomControl;
    private double _bottomControlLeft;
    private double _bottomControlBottom;

    
    private string _audioPath;
    private MediaPlayer _mediaPlayer;
    private LibVLC _libVLC;
    
    private bool _isAudioPlaying = false;


    public MainWindow()
    {
        InitializeComponent();
        
        this.AttachDevTools();
        
        _bottomControl = this.FindControl<Control>("BottomControl");
        _bottomControlLeft = 30;
        _bottomControlBottom = 60;

        
        //Configurações de som
        _libVLC = new LibVLC();
        var currentDirectory = Directory.GetCurrentDirectory();
        _audioPath = Path.Combine(currentDirectory, "Assets/fastinvader1.wav");
        _mediaPlayer = new MediaPlayer(_libVLC);
        _mediaPlayer.Media = new Media(_libVLC, new Uri(_audioPath));
        
        _mediaPlayer.EndReached += (sender, args) => AudioPlaybackEnded();
        
        KeyDown += MainWindow_KeyDown;

        DataContext = this;
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }

    private async void PlayMusic()
    {
        _mediaPlayer.Position = 0.0f;
        _mediaPlayer.Play();
        await Task.Delay(100);
        _isAudioPlaying = true;
    }

    private void StopMusic()
    {
        _mediaPlayer.Stop();
        _isAudioPlaying = false;
    }

    private void AudioPlaybackEnded()
    {
        if (_isAudioPlaying && !_mediaPlayer.IsPlaying)
        {
            PlayMusic();
        }
    }
    
    private void MainWindow_KeyDown(object sender, KeyEventArgs e)
    {
        const double moveStep = 30.0;

        switch (e.Key)
        {
            case Key.Left:
                MoveControl(-moveStep);
                break;
            case Key.Right:
                MoveControl(moveStep);
                break;
            case Key.Up:
                MoveControlVertical(moveStep);
                break;
            case Key.Down:
                MoveControlVertical(-moveStep);
                break;
        }
    }

    private async void MoveControl(double offset)
    {
        if (_bottomControlLeft + offset >= 30 && _bottomControlLeft + offset <= Width - 130)
        {
            StopMusic();
            await Task.Delay(100);
            PlayMusic();
            _bottomControlLeft += offset;
            Canvas.SetLeft(_bottomControl, _bottomControlLeft);  
        }
    }

    private async void MoveControlVertical(double offset)
    {
        if (_bottomControlBottom + offset >= 60 && _bottomControlBottom + offset <= Height - 60)
        {
            StopMusic();
            await Task.Delay(100);
            PlayMusic();
            _bottomControlBottom += offset;
            Canvas.SetBottom(_bottomControl, _bottomControlBottom);  
        }
    }
}
